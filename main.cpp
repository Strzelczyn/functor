#include <algorithm>
#include <iostream>

struct ObjDelete {
  int operator()(char i) const { return i == 'c'; }
};

bool methodDelete(char i) {
  return i == 'c';
}

int main() {
  std::string z = "abcde";
  std::string z1 = z;
  std::string z2 = z;

  remove_if(z.begin(), z.end(), [](char i) { return i == 'c'; });
  std::cout << z << std::endl;

  ObjDelete obj;
  remove_if(z1.begin(), z1.end(), obj);
  std::cout << z1 << std::endl;

  remove_if(z2.begin(), z2.end(), methodDelete);
  std::cout << z2 << std::endl;
}
